**Variation in post-smolt growth pattern of wild one sea-winter salmon (Salmo salar L.), and its linkage to surface warming in the eastern North Atlantic Ocean**

This repository aims to provide the code, sample data and figures supporting the publication "Variation in post-smolt growth pattern of wild one sea-winter salmon (Salmo salar L.), and its linkage to surface warming in the eastern North Atlantic Ocean".

Authors: Christopher D. Todd (1), Nora N. Hanson (2), Lars Boehme (1), Crawford W. Revie (3,4) and Ana R. Marques (3,5)

1. Scottish Oceans Institute, School of Biology, University of St Andrews, St Andrews, Fife, Scotland KY16 8LB.  cdt@st-andrews.ac.uk; lb284@st-andrews.ac.uk

2. Marine Scotland Science, Salmon and Freshwater Fisheries Laboratory, Pitlochry, Perthshire, Scotland PH16 5LB.  N.Hanson@marlab.ac.uk

3. Department of Health Management, Atlantic Veterinary College, University of Prince Edward Island, 550 University Avenue, Charlottetown, Prince Edward Island, Canada C1A 4P3

4. Department of Computer and Information Sciences, University of Strathclyde, Glasgow, Scotland G1 1XQ.  crawfordrevie@gmail.com

5. Research Group for Genomic Epidemiology, National Food Institute, Technical University of Denmark, Kemitorvet, Building 205 Room 104, 2800 Kgs. Lyngby, Denmark.  anaritamarques82@gmail.com



---
Corresponding author: C.D. Todd, cdt@st-andrews.ac.uk; Tel +44 1334 463454; Fax +44 1334 463443


---
Repository maintained by co-author Ana Rita Marques: anaritamarques82@gmail.com


